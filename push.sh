#!/bin/sh

IMAGE_NAME=$1

echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY || exit 1
docker build -t $CI_REGISTRY/apes2/a2images/$IMAGE_NAME:latest -f $IMAGE_NAME.dockerfile . || exit 1
docker push $CI_REGISTRY/apes2/a2images/$IMAGE_NAME:latest || exit 1