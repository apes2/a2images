ARG UBUNTU_VERSION=21.04

# Start with Ubuntu.
FROM ubuntu:${UBUNTU_VERSION}

ARG TERRAFORM_VERSION=0.15.5
ARG NODE_VERSION=14.17.0
ARG AWS_CLI_VERSION=2.0.30

# Install some needed packages.
RUN apt-get update
RUN apt-get install -y git unzip zip wget gcc xz-utils

# Install Terraform.
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN mv terraform /usr/bin
RUN rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Install Node.
RUN wget https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz
RUN tar -xf node-v${NODE_VERSION}-linux-x64.tar.xz
RUN mv node-v${NODE_VERSION}-linux-x64 /usr/bin/node
RUN rm node-v${NODE_VERSION}-linux-x64.tar.xz
ENV PATH="/usr/bin/node/bin:$PATH"

# Install the AWS CLI.
RUN wget https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWS_CLI_VERSION}.zip
RUN unzip awscli-exe-linux-x86_64-${AWS_CLI_VERSION}.zip
RUN ./aws/install
RUN rm awscli-exe-linux-x86_64-${AWS_CLI_VERSION}.zip
RUN rm -R ./aws